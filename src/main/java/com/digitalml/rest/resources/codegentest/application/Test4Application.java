package com.digitalml.rest.resources.codegentest.application;

import com.digitalml.rest.resources.codegentest.resource.Test4Resource;
import com.digitalml.rest.resources.codegentest.resource.ResourceLoggingFilter;					
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import com.codahale.metrics.health.HealthCheck;
import io.dropwizard.Configuration;

import java.util.EnumSet;
import javax.servlet.DispatcherType;

import org.slf4j.LoggerFactory;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.util.ContextInitializer;
import ch.qos.logback.core.joran.spi.JoranException;


public class Test4Application extends Application<Test4Configuration> {

	public static void main(final String[] args) throws Exception {
		new Test4Application().run(args);
	}
	
	@Override
	public void initialize(final Bootstrap<Test4Configuration> bootstrap) {
		// TODO: application initialization
	}

	@Override
	public void run(final Test4Configuration configuration, final Environment environment) {

		//separate logging setup to enable use of external logback xml
		LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
		context.reset();
		ContextInitializer initializer = new ContextInitializer(context);
		try {
			initializer.autoConfig();
		} catch (JoranException e) {
		}

		final Test4Resource resource = new Test4Resource();
		final Test4HealthCheck healthCheck = new Test4HealthCheck();
		
		environment.servlets().addFilter("ResourceLoggingFilter", new ResourceLoggingFilter()).addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, "/*");
		
		environment.healthChecks().register("template", healthCheck);
		environment.jersey().register(resource);

	}
}

final class Test4Configuration extends Configuration {
}

final class Test4HealthCheck extends HealthCheck {

	public Test4HealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		return Result.healthy();
	}
}